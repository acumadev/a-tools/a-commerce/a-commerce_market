export const environment = {
  all_commerces_url: 'https://services.a-ccount.acuma.dev/api/',
  products_url: 'https://products.a-commerce.acuma.dev/api/',
  orders_url: 'https://orders.a-commerce.acuma.dev/api/',
  production: true
};
