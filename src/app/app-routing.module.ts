import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommerceLayoutViewComponent } from './views/commerce-layout-view/commerce-layout-view.component'
import { CommerceListViewComponent } from './views/commerce-list-view/commerce-list-view.component'

const routes: Routes = [
  { path: "commerces/:commerce_name", component: CommerceLayoutViewComponent },
  { path: "commerces", component: CommerceListViewComponent },
  { path: '',   redirectTo: '/commerces'  , pathMatch: 'full' },
  { path: '**', redirectTo: '/commerces' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
