import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
'Angular Material Modules'
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

'QR Code Modules'
import { QRCodeModule } from 'angularx-qrcode';

'Loadout Directive'
import { ComponentLoaderDirective } from './directives/component-loader.directive';

'Layout Views'
import { CommerceLayoutViewComponent } from './views/commerce-layout-view/commerce-layout-view.component';

'Commerce Customable Items'
import { ProductBannerListComponent } from './components/product-banner-list/product-banner-list.component';
import { ProductGridListComponent } from './components/product-grid-list/product-grid-list.component';
import { CommerceListViewComponent } from './views/commerce-list-view/commerce-list-view.component';
import { ShoppingCartDialogComponent } from './dialogs/shopping-cart-dialog/shopping-cart-dialog.component';
import { AddProductSnackbarComponent } from './components/add-product-snackbar/add-product-snackbar.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentLoaderDirective,
    CommerceLayoutViewComponent,
    ProductBannerListComponent,
    ProductGridListComponent,
    CommerceListViewComponent,
    ShoppingCartDialogComponent,
    AddProductSnackbarComponent,
  ],
  imports: [
    FormsModule,
    QRCodeModule,
    BrowserModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatDividerModule,
    MatToolbarModule, 
    HttpClientModule,
    AppRoutingModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSlideToggleModule, 
    BrowserAnimationsModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
