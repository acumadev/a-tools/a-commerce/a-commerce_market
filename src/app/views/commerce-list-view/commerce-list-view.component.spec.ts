import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommerceListViewComponent } from './commerce-list-view.component';

describe('CommerceListViewComponent', () => {
  let component: CommerceListViewComponent;
  let fixture: ComponentFixture<CommerceListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommerceListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommerceListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
