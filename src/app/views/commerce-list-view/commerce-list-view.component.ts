import { Component, OnInit } from '@angular/core';
import { CommerceService } from '../../services/commerce.service';
@Component({
  selector: 'app-commerce-list-view',
  templateUrl: './commerce-list-view.component.html',
  styleUrls: ['./commerce-list-view.component.scss']
})
export class CommerceListViewComponent implements OnInit {

  commerces = []
  loaded = false
  constructor(private commerce: CommerceService) { }

  ngOnInit(): void {
    this.loaded = false
    this.commerce.getAllCommerces()
    .subscribe((data:any)=>{
      this.commerces = data.data
      this.loaded = true
    })
  }

}
