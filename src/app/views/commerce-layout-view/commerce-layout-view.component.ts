import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'
import { MatDialog } from '@angular/material/dialog';
import { CommerceService } from '../../services/commerce.service';
import { ComponentLoaderDirective } from '../../directives/component-loader.directive';
import { Component, OnInit, OnDestroy, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { ShoppingCartDialogComponent } from '../../dialogs/shopping-cart-dialog/shopping-cart-dialog.component'
import { ProductBannerListComponent } from '../../components/product-banner-list/product-banner-list.component'
import { ProductGridListComponent } from '../../components/product-grid-list/product-grid-list.component'

@Component({
  selector: 'app-commerce-layout-view',
  templateUrl: './commerce-layout-view.component.html',
  styleUrls: ['./commerce-layout-view.component.scss']
})
export class CommerceLayoutViewComponent implements OnInit, OnDestroy {

  commerce_info
  commerce_name 
  commerce_id
  qr_code_url 
  sidenav = false
  components = ["product-grid"]
  @ViewChild(ComponentLoaderDirective, {static: true}) componentLoader: ComponentLoaderDirective;
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private commerce: CommerceService,
    private route: ActivatedRoute,
    public location: Location,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.qr_code_url = window.location.href
    this.route.paramMap.subscribe(params => {
      this.commerce_name = params.get('commerce_name')
      this.commerce.getCommerceID(params.get('commerce_name'))
      .subscribe((data:any)=> {
        this.commerce_id = data.id
        this.commerce_info = data
        this.loadComponents(data.id)
      })
    });
  }

  ngOnDestroy() {
  }

  openShoppingCart() {
    this.dialog.open(ShoppingCartDialogComponent, {
      //width: (window.innerWidth*.8)+'px',
      //height: (window.innerHeight*.8)+'px',
      data: { commerce_id: this.commerce_id, delivery_service: this.commerce_info.delivery_service }
    });
  }

  loadComponents(commerce_id){
    for(let component of this.components){
      let componentFactory
      componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.returnComponentType(component));
      let viewContainerRef = this.componentLoader.viewContainerRef;
      let componentRef = viewContainerRef.createComponent(componentFactory);
      (<any>componentRef.instance).commerce_id = commerce_id;
    }
  }

  returnComponentType(component){
    switch(component) {
      case "product-grid":
        return ProductGridListComponent
      case "product-banner":
        return ProductBannerListComponent
      default:
        return null
    }
  }
}
