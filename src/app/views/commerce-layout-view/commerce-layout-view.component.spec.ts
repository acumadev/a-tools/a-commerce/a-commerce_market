import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommerceLayoutViewComponent } from './commerce-layout-view.component';

describe('CommerceLayoutViewComponent', () => {
  let component: CommerceLayoutViewComponent;
  let fixture: ComponentFixture<CommerceLayoutViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommerceLayoutViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommerceLayoutViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
