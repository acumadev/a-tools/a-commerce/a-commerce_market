import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-product-snackbar',
  templateUrl: './add-product-snackbar.component.html',
  styleUrls: ['./add-product-snackbar.component.scss']
})
export class AddProductSnackbarComponent implements OnInit {

  constructor(
    public snackbar: MatSnackBar,
  ) { }

  ngOnInit(): void {
  }

}
