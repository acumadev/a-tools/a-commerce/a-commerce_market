import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProductSnackbarComponent } from './add-product-snackbar.component';

describe('AddProductSnackbarComponent', () => {
  let component: AddProductSnackbarComponent;
  let fixture: ComponentFixture<AddProductSnackbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProductSnackbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProductSnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
