import { AddProductSnackbarComponent } from '../../components/add-product-snackbar/add-product-snackbar.component';
import { CommerceService } from '../../services/commerce.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-product-grid-list',
  templateUrl: './product-grid-list.component.html',
  styleUrls: ['./product-grid-list.component.scss']
})
export class ProductGridListComponent implements OnInit {
  @Input() commerce_id: any;

  products = []
  loaded = false
  constructor(
    private snackbar: MatSnackBar,
    private commerce: CommerceService
  ) { }

  ngOnInit(): void {
    this.loaded = false
    this.commerce.getCommerceData(this.commerce_id)
    .subscribe((data:any)=>{
      this.products = data.data
      this.loaded = true
    })
  }

  addToCart(product){
    let cart = sessionStorage.getItem('a_commerce_shopping_cart')
    if(cart){
      let products = cart.split(',')
      let index = products.indexOf((product.id).toString())
      if(index < 0) {
        products.push(product.id)
        sessionStorage.setItem('a_commerce_shopping_cart', products.toString())
      }
    }else{
      console.log(product)
      let products = []
      products.push(product.id)
      sessionStorage.setItem('a_commerce_shopping_cart', products.toString())
    }
    this.snackbar.openFromComponent(AddProductSnackbarComponent, {
      duration: 5 * 1000,
      verticalPosition: 'top',
    });
  }

}
