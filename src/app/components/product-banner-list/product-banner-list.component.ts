import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product-banner-list',
  templateUrl: './product-banner-list.component.html',
  styleUrls: ['./product-banner-list.component.scss']
})
export class ProductBannerListComponent implements OnInit {
  @Input() commerce_id: any;
  constructor() { }

  ngOnInit(): void {
    console.log(this.commerce_id)
  }

}
