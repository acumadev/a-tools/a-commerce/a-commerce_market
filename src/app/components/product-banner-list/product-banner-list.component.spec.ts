import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductBannerListComponent } from './product-banner-list.component';

describe('ProductBannerListComponent', () => {
  let component: ProductBannerListComponent;
  let fixture: ComponentFixture<ProductBannerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductBannerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductBannerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
