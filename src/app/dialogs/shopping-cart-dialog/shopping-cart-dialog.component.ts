import { Component, OnInit, Inject } from '@angular/core';
import { OrderService } from '../../services/order.service';
import { CommerceService } from '../../services/commerce.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommerceLayoutViewComponent } from '../../views/commerce-layout-view/commerce-layout-view.component'
@Component({
  selector: 'app-shopping-cart-dialog',
  templateUrl: './shopping-cart-dialog.component.html',
  styleUrls: ['./shopping-cart-dialog.component.scss']
})
export class ShoppingCartDialogComponent implements OnInit {

  unit = ""
  phone = ""
  address = ""
  products = []
  loaded = false
  created: boolean
  references = ""
  unit_error = false
  empty_error = false
  delivery_service = false
  units = ['Kilogramo(s)', 'Gramo(s)', 'Pieza(s)']
  displayedColumns: string[] = ['Producto', 'Cantidad', 'Unidad', "Precio"];
  constructor(
    public dialogRef: MatDialogRef<CommerceLayoutViewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commerce: CommerceService,
    private order: OrderService
  ) { }

  ngOnInit(): void {
    this.loaded = false
    let products = sessionStorage.getItem('a_commerce_shopping_cart')

    if(products){
      let shopping_cart = products.split(',')
      this.commerce.getShoppingCart(this.data.commerce_id, shopping_cart)
      .subscribe((data:any) =>{
        for(let product of data.data){
          product["quantity"] = 1
        }
        this.products = data.data
        this.loaded = true
      })
    }else{
      this.loaded = true
    }
  }
  
  removeQuantity(element) {
    if(element.quantity > 0){
      element.quantity--
    }else{
      let index = this.products.indexOf(element)
      console.log(index)
    }
  }

  createOrder(name,lastname) {
    let user 
    this.unit_error = false
    this.empty_error = false
    if(this.delivery_service){
      user = {
        name: name.value,
        phone: this.phone,
        address: this.address,
        lastname: lastname.value,
        references: this.references,
        delivery_service: this.delivery_service,
      }
    }else{
      user = {
        name: name.value,
        lastname: lastname.value,
        delivery_service: this.delivery_service,
      }
    }
    for(let key in user){
      if(typeof user[key] === "string"){
        if(user[key].trim() === ""){
          this.empty_error = true
        }
      }
    }
    let products = []
    for(let product of this.products){
      if(product.unit){
        products.push({
          name: product.name, 
          quantity:product.quantity+" "+product.unit,
        })
      }else{
        this.unit_error = true
      }
    }
    if (!this.unit_error && !this.empty_error) {
      let order = {user:user, products:products}
      this.order.createOrder(this.data.commerce_id, order)
      .subscribe((data:any) => {
        console.log(data)
        if(data.data){
          this.created = true
        }
      })
    }
  }

}
