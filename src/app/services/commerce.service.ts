import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommerceService {
  commerces_url = environment.all_commerces_url
  products_url = environment.products_url

  constructor(private http: HttpClient) { }

  getAllCommerces(){
    return this.http.get(this.commerces_url+'commerces')
      .pipe(map(commerces => {
      return commerces;
    }));
  }

  getCommerceID(commerce_name){
    return this.http.get(this.commerces_url+'commerce/'+commerce_name)
      .pipe(map(commerce => {
      return commerce;
    }));
  }

  getCommerceData(commerce_id){
    return this.http.get(this.products_url+commerce_id+'/products')
      .pipe(map(commerce => {
      return commerce;
    }));
  }

  getShoppingCart(commerce_id, data) {
    return this.http.post(this.products_url+commerce_id+'/shopping-cart/products',{products:data})
      .pipe(map(cart => {
      return cart;
    }));
  }

}
