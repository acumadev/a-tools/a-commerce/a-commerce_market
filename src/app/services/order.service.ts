import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  orders_url = environment.orders_url

  constructor(private http: HttpClient) { }

  createOrder(commerce_id, data){
    return this.http.post(this.orders_url+commerce_id+'/orders',{"order": data})
      .pipe(map(order => {
      return order;
    }));
  }
}
